#!/usr/bin/bash

Runs=$1
BLTSize=$2

for ii in `seq $Runs`;do for bsize in `seq 40 40 600`; do ./vx1730_Run --runtype swtrigger -n 1000 -L $bsize -B $BLTSize --config ../configs/cfg_vx1730.json|tail -n 2|head -n1|cut -f2 -d\|;done;done > rates_bsize${BLTSize}.dat
cat rates_bsize${BLTSize}.dat|cut -f4,8,12 -d' ' > rates_bsize${BLTSize}.nt
