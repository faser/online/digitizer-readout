# 
# Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
# 
#!/usr/bin/env python3

import getopt
import socket
import sys
import time

class TTI:
    def __init__(self,ip,port):
        s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect( (ip,port) )
        self.socket=s

    def send(self,cmd):
        print("Sending:",cmd)
        self.socket.sendall(bytes(cmd+'\n','utf8'))


def usage():
    print("pulse.py [-f <rate in Hz>] [-w <pulse width in ns>] <amplitude in volt> [Number of pulses]")
    sys.exit(1)
        
def main(args):
    width=5e-8 # seconds
    freq=100   # Hz
    ampl=1.2   # V
    numPulses=1 # count
    try:
      opts, args = getopt.getopt(args,"f:w:",[])
    except getopt.GetoptError:
        usage()
    if len(args)<1: usage()

    for opt,arg in opts:
        if opt=="-w":
            width=int(arg)*1e-9
        if opt=="-f":
            freq=int(arg)
    ampl=float(args[0])
    if len(args)>1:
        numPulses=int(args[1])


    tti=TTI("ttifaser.dyndns.cern.ch",9221)
    tti.send('WAVE PULSE')
    tti.send('AMPUNIT VPP')
    tti.send('BST OFF')
    tti.send('PULSWID %.8f' % width)
    tti.send('PULSFREQ %d' % freq)
    tti.send('AMPL %.3f' % ampl)
    tti.send('DCOFFS %.3f' % (ampl/2))
    tti.send('SYNCTYPE CARRIER')
    tti.send('OUTPUT ON')
#    tti.send('*TRG')
#    time.sleep(1.0+1.*numPulses/freq)
#    tti.send('LOCAL')

if __name__ == '__main__':
    main(sys.argv[1:])
