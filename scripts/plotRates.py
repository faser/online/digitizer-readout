#!/usr/bin/env python3
# 
# Copyright (C) 2019-2021 CERN for the benefit of the FASER collaboration
# 

import sys

import ROOT

def main(args):
    ntuples=[]
    for blt in args:
        nt=ROOT.TNtuple(f"nt{blt}","","rate/F:bw/F:size/F")
        fname=f"rates_bsize{blt}.nt"
        num=nt.ReadFile(fname)
        if num==0:
            print(f"Failed to read {fname}")
            sys.exit(1)
        ntuples.append(nt)

    ROOT.gStyle.SetOptStat(0)
    cv=ROOT.TCanvas("cv","")
    cv.SetTicks()
    tl=ROOT.TLegend(0.65,0.65,0.9,0.9)
    opt="PROFS"
    col=1
    
    for idx,blt in enumerate(args):
        nt=ntuples[idx]
        nt.SetMarkerColor(col)
        nt.SetMarkerStyle(20)
        nt.SetFillColor(col)
        nt.Draw("rate/1000:size/1024","size>2000",opt)
        hist=nt.GetHistogram()
        hist.SetTitle("")
        hist.SetXTitle("Event size [kBytes]")
        hist.SetYTitle("Readout rate [kHz]")
        hist.GetYaxis().SetTitleOffset(1.02)
        tl.AddEntry(nt,f"{blt} events per read","F")
        opt="SAMEPROFS"
        col+=1
    tl.Draw()
    cv.Print("DigitizerRate.pdf")

if __name__ == "__main__":
    main(sys.argv[1:])
