# 
# Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
# 
#!/usr/bin/env python3

import getopt
import socket
import sys
import time

class TTI:
    def __init__(self,ip,port):
        s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect( (ip,port) )
        self.socket=s

    def send(self,cmd):
        print("Sending:",cmd)
        self.socket.sendall(bytes(cmd+'\n','utf8'))


        
def main():

    tti=TTI("ttifaser.dyndns.cern.ch",9221)
    tti.send('OUTPUT OFF')

if __name__ == '__main__':
    main()
