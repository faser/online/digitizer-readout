/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Comm_vx1730.h"
#include "Helper.h"
#include "nlohmann/json.hpp"

class digitizerHandler {
   public:
    digitizerHandler(char ip[], const nlohmann::json config);

    void perform_interface_speedtest(unsigned int nreads);
    void test_communication();
    void dump_config();

    void configure_ethernet_transmission();
    void configure(bool debug = false);
    void start_acquisition(bool debug = false);
    void send_sw_trigger(bool debug = false);
    void stop_acquisition(bool debug = false);

    std::vector<size_t> get_event_sizes();
    size_t n_digitizers() { return m_nbDigitizers; }

    void read_single_event(size_t d_id, READOUT &readout, unsigned int n_events_to_do, bool debug = false);

    size_t dump_event_count(bool debug, size_t d_id);
    void dump_event_count(std::vector<UINT> &occupancies, bool debug = false);

    std::vector<std::unique_ptr<vx1730>> m_digitizers;
    shared_ptr<sis3153eth> m_crate;

   private:
    nlohmann::json m_config;
    size_t m_nbDigitizers;  // same as m_digitizers.size()
};
