/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

#include "project_system_define.h"		//define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)

#include "Logging.hpp"

#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>

#include "sis3153usb.h"
#include "vme_interface_class.h"

#include "sis3153ETH_vme_class.h"
#include <sys/types.h>
#include <sys/socket.h>

//Addresses for PFC8584 controller
#define DR_PFC8584 0x11
#define CR_PFC8584 0x13
#define SR_PFC8584 0x13

//Control bits for PFC8584 controller
#define ACK_MASK 0x01 
#define STO_MASK 0x02 
#define STA_MASK 0x04 
#define ENI_MASK 0x08 
#define ES2_MASK 0x10 
#define ES1_MASK 0x20 
#define ESO_MASK 0x40 

//Status bits for PFC8584 controller
#define BB_MASK 0x01 
#define LAB_MASK 0x02 
#define AAS_MASK 0x04 
#define LRB_MASK 0x08 
#define BER_MASK 0x10 
#define STS_MASK 0x20 
#define PIN_MASK 0x80


class BOBR
{
public:
  BOBR(sis3153eth *vme_crate, bool verbose=false) { 
    _vme=vme_crate; 
    _verbose=verbose; 
  };
  int readBlock0Register(unsigned int address, unsigned char* cdata) {
    unsigned int addr=block0_base+address;
    int rc = _vme->vme_A16D8_read (addr, cdata);
    if (_verbose) 
      printf("readBlock0Register: addr = 0x%08X    data = 0x%02X    return_code = 0x%08X \n", addr, *cdata,rc);

    return rc;
  };
  int writeBlock0Register(unsigned int address, unsigned char cdata) {
    unsigned int addr=block0_base+address;
    int rc = _vme->vme_A16D8_write (addr, cdata);
    if (_verbose) 
      printf("writeBlock0Register: addr = 0x%08X    data = 0x%02X    return_code = 0x%08X \n", addr, cdata,rc);
    return rc;
  };
  int readPFCData(unsigned char* cdata) {
    return readBlock0Register(DR_PFC8584,cdata);
  };
  int readPFCStatus(unsigned char* cdata) {
    return readBlock0Register(SR_PFC8584,cdata);
  };
  int writePFCData(unsigned char cdata) {
    return writeBlock0Register(DR_PFC8584,cdata);
  };
  int writePFCControl(unsigned char cdata) {
    return writeBlock0Register(CR_PFC8584,cdata);
  };
  int initPFC() {
    if (_verbose) printf("initializing PFC (I2C controller)\n");
    int rc=0;
    rc=writePFCControl(PIN_MASK);
    if (rc) { printf("Failed to switch to S0\n"); return rc; }
    rc=writePFCData(0x55);
    if (rc) { printf("Failed to set own address\n"); return rc; }
    rc=writePFCControl(PIN_MASK|ES1_MASK);
    if (rc) { printf("Failed to switch to S2\n"); return rc; }
    rc=writePFCData(0x18);  //Serial Clock frequency = 90 KHz ; System clock frequency = 8 MHz.
    if (rc) { printf("Failed to set clocks\n"); return rc; }
    rc=writePFCControl(PIN_MASK|ESO_MASK|ACK_MASK);
    if (rc) { printf("Failed to switch to standby\n"); return rc; }

    unsigned char cdata;
    rc=readPFCStatus(&cdata);
    if (rc) { printf("Failed to read status\n"); return rc; }
    if (_verbose) printf("PFC status bits: 0x%02X\n",cdata);
    usleep(10000);
    rc=readPFCStatus(&cdata);
    if (rc) { printf("Failed to read status\n"); return rc; }
    if (_verbose) printf("PFC status bits: 0x%02X\n",cdata);
    return rc;
  };
  int waitReady(bool pinOnly=false,int retries=6) {
    unsigned char status;
    int rc;
    while(retries>0) {
      rc=readPFCStatus(&status);
      if (rc) { printf("Failed to get PFC status\n"); return rc; }
      if ((status&PIN_MASK)==0) {
	if (pinOnly) return 0;
	if ((status&LRB_MASK)==0) {
	  return 0;
	} else {
	  printf("Did not get LRB signal\n");
	  return 1;
	}
      }
      //      usleep(100);
      retries--;
    }
    printf("PFC failed to become ready\n");
    return 1;
  };
  int i2cWrite(unsigned char address,unsigned char cdata) {
    int rc;
    unsigned char status;
    if (_verbose) printf("I2C-write %02x to address %02x\n",cdata,address);
    rc=readPFCStatus(&status);
    if (rc) { printf("Failed to get PFC status\n"); return rc; }
    if ((status&BB_MASK)==0) {
      printf("Bus busy - skipping\n");
      return 1;
    }
    rc=writePFCData(address*2);
    if (rc) { printf("Failed to set i2c address\n"); return rc; }
    rc=readPFCStatus(&status);
    if (rc) { printf("Failed to get PFC status\n"); return rc; }

    rc=writePFCControl(PIN_MASK|ESO_MASK|STA_MASK|ACK_MASK);
    if (rc) { printf("Failed to start i2c communication\n"); return rc; }
    rc=waitReady();
    if (rc) { 
      printf("Failed to get i2c response\n"); 
      writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
      return rc; 
    }
    rc=writePFCData(cdata);
    if (rc) { 
      printf("Failed to send i2c data\n"); 
      writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
      return rc; 
    }
    rc=waitReady();
    if (rc) { printf("Failed to get i2c response to data word\n"); }
    rc|=writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
    if (rc) { printf("Failed to stop i2c transmission\n"); }
    return rc;
  };
  int i2cRead(unsigned char address,unsigned char* cdata) {
    int rc;
    if (_verbose) printf("I2C-read from address %02x\n",address);
    rc=writePFCData(address*2+1);
    if (rc) { printf("Failed to set i2c address\n"); return rc; }
    rc=writePFCControl(PIN_MASK|ESO_MASK|STA_MASK|ACK_MASK);
    if (rc) { printf("Failed to start i2c communication\n"); return rc; }
    rc=waitReady();
    if (rc) { printf("Failed to get i2c response\n"); return rc; }

    rc=writePFCControl(PIN_MASK|ESO_MASK);
    if (rc) { 
      printf("Failed to acknowledge i2c data\n"); 
      writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
      return rc; 
    }
    rc=readPFCData(cdata); //dummy word - to be ignored
    if (rc) { 
      printf("Failed to get dummy i2c data\n"); 
      writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
      return rc; 
    }
    rc=waitReady(true);
    if (rc) { printf("Failed to get i2c response to read word\n"); }
    rc|=writePFCControl(PIN_MASK|ESO_MASK|STO_MASK|ACK_MASK);
    if (rc) { printf("Failed to stop i2c transmission\n"); }
    if (!rc) {
      rc=readPFCData(cdata);
      if (rc) { printf("Failed to read i2c data\n"); }
    }
    return rc;
  };
    
  int readTTCRegister(int address,unsigned char* cdata) {
    int rc;
    rc=i2cWrite(ttc_base,address);
    if (rc) { printf("Failed to transmit address\n"); return rc; }
    rc=i2cRead(ttc_base+1,cdata);
    if (rc) { printf("Failed to read TTC register\n"); return rc; }
    if (_verbose) { printf("Read TTC register 0x%02X: value = 0x%02X\n",address,*cdata); }
    return rc;
  }

  int writeTTCRegister(int address,unsigned char cdata) {
    int rc;
    rc=i2cWrite(ttc_base,address);
    if (rc) { printf("Failed to transmit address\n"); return rc; }
    rc=i2cWrite(ttc_base+1,cdata);
    if (rc) { printf("Failed to write TTC register\n"); return rc; }
    if (_verbose) { printf("Wrote to TTC register 0x%02X: value = 0x%02X\n",address,cdata); }
    return rc;
  }

  int readLockStatus(unsigned int* status) {
    unsigned int data;
    int rc = _vme->udp_sis3153_register_read (SIS3153USB_LEMO_IO_CTRL_REG, &data);
    if (rc!=0) {
      printf("Failed to read clock status, status code: 0x%08X\n",rc);
      return rc;
    }
    *status=(data&(1<<20))!=0;
    return rc;
  } 
private:
  sis3153eth *_vme;
  bool _verbose;
  const unsigned int block0_base = 0x0000B000;
  const int ttc_base = 0x02;

};

