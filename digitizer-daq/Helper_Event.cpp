/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper_Event.cpp
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper functions for dealing with an event in software.
 *
 * Once an event has been read off the board, there are many things you may with
 * to do with it.  For example, you may wish to query some particular information encoded within the 
 * event or you may with to write this event out for inspection.  This provides methods to allow you
 * to do that more easily.
 * 
 */
 
#include "Helper_Event.h"

/*!
 * \brief Get event size
 *
 * Decode the event payload header to obtain the number of words of the event payload.
 *
 * \param[in] [raw_payload] The payload obtained from the digitizer hardware.
 * \param[in] [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return The number of words in the event payload.
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 */
unsigned int Payload_GetEventSize( const uint32_t raw_payload[], [[maybe_unused]] bool debug  ){
  unsigned int eSize = raw_payload[0] & 0x0FFFFFF;
  return eSize;
}

/*!
 * \brief Get channel mask
 *
 * Decode the event payload header to obtain the set of channels that were configured for readout.
 *
 * \param[in]  [raw_payload] The payload obtained from the digitizer hardware.
 * \param[out] [channel_mask] The list of channels that were configured for readout upon an acquisition trigger.
 * \param[in]  [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 * \todo Make this a void function - what is the output used for?
 */
int Payload_GetChannelMask( const uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug){
  INFO("[] Payload_GetChannelMask");

  channel_mask.clear();

  // assume a channel is not enabled
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    channel_mask.push_back(false);
  }

  // save the channel masks
  // TODO : Make the number of channels not hardcoded
  for(unsigned int iChan=0; iChan<8; iChan++){
    channel_mask.at(iChan) = (raw_payload[1] & (1<<iChan)) >> iChan;
  }

  for(unsigned int iChan=8; iChan<16; iChan++){
    channel_mask.at(iChan) = (raw_payload[2] & (1<<(16+iChan))) >> (16+iChan);
  }

  // print out the channel mask in debug mode
  if(debug){
    for(size_t iChan=0; iChan<NCHANNELS; iChan++){
      INFO("ChannelMask : "<<channel_mask.at(iChan));
    }
  }

  return 0;
}

/*!
 * \brief Get the acquisition window length
 *
 * Decode the event payload header to obtain the length of the acquisition window that were configured for readout.  This is made available in terms of the number of words as well as the number of samples to which this corresponds.  For the digitizer, this should correspond to a factor of 2 difference because each word is 32 bits long, but each reading is only 14 bits long and so two readings are formatted in one word.
 *
 * \param[in]  [raw_payload]         The payload obtained from the digitizer hardware.
 * \param[out] [words_per_channel]   The number of words in the payload of the event, excluding the header (4 words).
 * \param[out] [samples_per_channel] The number of ADC readings in this acquisition window.
 * \param[in]  [debug]               A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 * \todo Make this a void function - what is the output used for?
 */
int Payload_GetBufferLength( const uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, [[maybe_unused]] bool debug){
  INFO("[] Payload_GetBufferLength");

  // get channel mask
  std::vector<bool> temp_channel_mask;
  Payload_GetChannelMask(raw_payload, temp_channel_mask, true);

  // find number of summed channels
  unsigned int n_channel_active=0;
  for(size_t iChan=0; iChan<temp_channel_mask.size(); iChan++){
    if(temp_channel_mask.at(iChan)==true)
      n_channel_active++;
  }

  // get event size
  unsigned int eSize=Payload_GetEventSize( raw_payload );

  // subtract 4 for the header
  unsigned int eSizeMod = eSize-4u;

  // divide modified event size by number of channels
  words_per_channel = static_cast<int>(eSizeMod/n_channel_active);

  samples_per_channel = 2*words_per_channel;

  return 0;
}

/*!
 * \brief Print the payload content
 *
 * Dump the raw payload to the screen without much decoding
 *
 * \param[in]  [raw_payload]         The payload obtained from the digitizer hardware.
 * \param[in]  [debug]               A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 * \todo Make this a void function - what is the output used for?
 * \todo Do some rudimentary decoding to make the dump of this more interpretable.
 */
int Payload_Dump( const uint32_t raw_payload[], bool debug ){
  INFO("[] Payload_Dump");

  unsigned int EventSize = Payload_GetEventSize( raw_payload, debug );
  
  INFO("Payload_Dump EventSize : "<<EventSize);

  INFO("DUMP - Header");
  for(int iWord=0; iWord<4; iWord++){
    INFO("head "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]);
  }

  INFO("DUMP - Data");
  for(unsigned int iWord=4; iWord<EventSize; iWord++){
    INFO("data "<<std::dec<<iWord<<" : "<<std::setfill('0')<<std::setw(8)<<std::hex<<raw_payload[iWord]);
  }

  return 0;
}

/*!
 * \brief Parse the event payload
 *
 * Decode the event payload header to obtain the digitizer event data, the ADC readings, in a useable format as a vector of vectors, one for each channel that is enabled.
 *
 * \param[in]  [raw_payload] The payload obtained from the digitizer hardware.
 * \param[out] [data_parsed] The vector of vectors of the parsed and decoded event data.  Each entry in this vector is itself a vector of the ADC readings for each of the enabled channel.
 * \param[in]  [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 * \warning This only saves data for enabled channels.
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 * \todo Make this a void function - what is the output used for?
 * \todo Make this more aware of which channels were enabled or not. Perhaps by changing the return value to be a map instead of a vector of vectors.
 */
int Payload_Parse(const  uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, [[ maybe_unused ]] bool debug){
  INFO("[] Payload_Parse");

  // clear the data that was in the output
  data_parsed.clear();

  // int eSize = Payload_GetEventSize(raw_payload);

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(raw_payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(raw_payload, words_per_channel, samples_per_channel, true);

  INFO("Samples : "<<samples_per_channel<<"  "<<words_per_channel);

  // TODO - put some checks that the number of channels and the buffer length makes sense

  // now parse the event data buffer depending on the channel mask info
  int currentLoc=4;

  for(unsigned int iChan=0; iChan<NCHANNELS; iChan++){

    INFO("Location : "<<iChan<<" "<<currentLoc);

    data_parsed.push_back({});

    if(channel_mask.at(iChan)==1){
      INFO("Channel good to read : "<<iChan<<" start = "<<currentLoc);
      for(int iDat=currentLoc; iDat<currentLoc+words_per_channel; iDat++){

        // two readings are stored in one word
        unsigned int chData = raw_payload[iDat];

        unsigned int ev0    = (chData & 0xFFFF0000) >> 16;  // first half of event doublet
        unsigned int ev1    = (chData & 0x0000FFFF);        // second half of event doublet

        data_parsed.at(iChan).push_back( ev1 );
        data_parsed.at(iChan).push_back( ev0 );

      }

      // move where we are looking
      currentLoc += words_per_channel;
    }

  }



  return 0;
}

/*!
 * \brief Dump partial event
 *
 * Show just a few words to inspect the quality of the data after the payload has been packaged into a FASER-style fragment.
 *
 * \param[in]  [data] The FASER-style event fragment
 * \param[in]  [n_dump] The number of words to dump from the digitizer payload.
 * \param[in]  [debug]  [A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 * \warning This only saves data for enabled channels.
 *
 * \todo This assumes it was a successful read with a good header.  Perhaps first you should query the header to assume this was correct.
 * \todo Use smart pointers instead of C-style memory.
 * \todo Make this a void function - what is the output used for?
 */
int Data_Dump( std::unique_ptr<EventFragment>& data, unsigned int n_dump, [[ maybe_unused ]] bool debug){
  INFO("[] Data_Dump");

  INFO("Header(FASER)");

  INFO(" fragment_tag   - "<<std::hex<<unsigned(data->fragment_tag())   );
  INFO(" trigger_bits   - "<<std::hex<<data->trigger_bits()   );
  INFO(" payload_size   - "<<std::hex<<data->payload_size()   );
  INFO(" total size     - "<<std::hex<<data->size()           );
  INFO(" source_id      - "<<std::hex<<data->source_id()      );
  INFO(" event_id       - "<<std::hex<<data->event_id()       );
  INFO(" bc_id          - "<<std::hex<<data->bc_id()          );
  INFO(" status         - "<<std::hex<<data->status()         );
  INFO(" timestamp      - "<<std::hex<<data->timestamp()      );

  INFO("\nData Payload - Header");
  const uint32_t *payload=data->payload<const uint32_t*>();
  
  for(int iPay=0; iPay<4; iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }

  INFO("\nData Payload - Data ["<<n_dump<<" words] Beginning");
  for(unsigned int iPay=4; iPay<4+n_dump; iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }

  INFO("\nData Payload - Data ["<<n_dump<<" words] End");
  for(uint32_t iPay=data->payload_size()-n_dump; iPay<data->payload_size(); iPay++){
    INFO(" "<<std::dec<<iPay<<" : "<<std::hex<<unsigned(payload[iPay]));
  }
  return 0;
}

/*!
 * \brief Write the event data to a file
 *
 * Starting from an event payload which has been packaged into a FASER-style fragment, write the event to a text file in a humen-interpretable format.
 *
 * \param[in]  [data] The FASER-style event fragment
 * \param[in]  [outputpath] The file path to which the data will be written.
 * \param[in]  [mode] One of The enum DumpMode which can take values {New, Append} and refers to whether the file will be freshly created, or each subsequent event will be appended onto the same text file.
 * \param[in]  [debug]  A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 * \return 0 - this is a silly and pointless return value
 *
 * \todo Make this a void function - what is the output used for?
 */
int Data_Write( std::unique_ptr<EventFragment>& data, std::string outputpath, DumpMode mode, bool debug){
  INFO("\n\n[] Data_Write : "<<outputpath);

  std::ofstream fout;
  
  if(mode == DumpMode::New){
    INFO("Making new file : "<<outputpath);
    fout.open(outputpath.c_str(), std::ios::out);
  }
  else if(mode == DumpMode::Append){
    INFO("Appending to file : "<<outputpath);
    fout.open(outputpath.c_str(), std::ios::app);
  }
  else{
    INFO("This is not a valid write method");
  }
  
  fout<<std::endl<<"<event>"<<std::endl;

  if(debug) INFO("\nHeader(FASER)");
  //  fout<<"faser-marker         - "<<std::hex<<unsigned(data->header.marker)         <<std::endl;
  fout<<"faser-fragment_tag   - "<<std::hex<<unsigned(data->fragment_tag())   <<std::endl;
  fout<<"faser-trigger_bits   - "<<std::hex<<data->trigger_bits()             <<std::endl;
  //  fout<<"faser-version_number - "<<std::hex<<data->header.version_number           <<std::endl;
  //  fout<<"faser-header_size    - "<<std::hex<<data->header.header_size              <<std::endl;
  fout<<"faser-payload_size   - "<<std::hex<<data->payload_size()             <<std::endl;
  fout<<"faser-source_id      - "<<std::hex<<data->source_id()                <<std::endl;
  fout<<"faser-event_id       - "<<std::hex<<data->event_id()                 <<std::endl;
  fout<<"faser-bc_id          - "<<std::hex<<data->bc_id()                    <<std::endl;
  fout<<"faser-status         - "<<std::hex<<data->status()                   <<std::endl;
  fout<<"faser-timestamp      - "<<std::hex<<data->timestamp()                <<std::endl;

  const uint32_t *payload=data->payload<const uint32_t* >();
  if(debug) INFO("\nData Payload - Header");
  fout<<"payload-header0 : "<<std::hex<<unsigned(payload[0])<<std::endl;
  fout<<"payload-header1 : "<<std::hex<<unsigned(payload[1])<<std::endl;
  fout<<"payload-header2 : "<<std::hex<<unsigned(payload[2])<<std::endl;
  fout<<"payload-header3 : "<<std::hex<<unsigned(payload[3])<<std::endl;


  std::vector< std::vector< unsigned int >> parsed_data;
  Payload_Parse(payload, parsed_data );

  std::vector<bool> channel_mask;
  Payload_GetChannelMask(payload, channel_mask, true);

  int words_per_channel;
  int samples_per_channel;
  Payload_GetBufferLength(payload, words_per_channel, samples_per_channel, true);

  if(debug)
    INFO("Data Payload");

  // reading value
  fout<<"payload-data "<<std::setw(10)<<"reading";
  // write data for each channel that is active and -1 if its not active
  for(int iChan=0; iChan<NCHANNELS; iChan++){
    std::string line = "chan-"+std::to_string(iChan);
    fout<<std::setw(13)<<line;
  }
  fout<<"\n";

  for(size_t iData=0; iData< static_cast<size_t>(samples_per_channel); iData++){

    if(debug)
      INFO("Writing out : "<<iData);

    // reading value
    fout<<"PAYLOAD-DATA "<<std::setw(10)<<std::dec<<iData<<"   ";

    // write data for each channel that is active and -1 if its not active
    for(size_t iChan=0; iChan<NCHANNELS; iChan++){
      if(channel_mask.at(iChan)==1){
        fout<<std::setw(10)<<parsed_data.at(iChan).at(iData)<<"   ";
      }
      else{
        fout<<std::setw(10)<<"-1"<<"   ";
      }
    }

    // write a newline
    fout<<"\n";

  }

  fout.close();

  return 0;
}
