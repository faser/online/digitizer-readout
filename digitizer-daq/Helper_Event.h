/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper_Event.h
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper methods for processing event after readout
 *
 */
 
#ifndef  HELPER_EVENT_INCLUDE_H
#define  HELPER_EVENT_INCLUDE_H

#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <cstring>
#include <memory>
#include <fstream>

#include "Helper.h"

#include "EventFormats/DAQFormats.hpp"
#include "EventFormats/DigitizerDataFragment.hpp"

using namespace DAQFormats;

// this controls the size of the software buffer 
// a max of 200000 seems stable and making it larger
// gives a segfault by allocating too much memory
#define MAXFRAGSIZE 500000
#define NCHANNELS 16

// emulation of the FASER-DAQ event fragment structures
// remove this later
// enum EventMarkers {
//   FragmentMarker = 0xAA,
//   EventMarker = 0xBB
// };

enum DumpMode {
 New,
 Append
};


unsigned int Payload_GetEventSize(const uint32_t raw_payload[], bool debug=false );
int Payload_GetChannelMask(const uint32_t raw_payload[], std::vector< bool >& channel_mask, bool debug=false );
int Payload_GetBufferLength(const uint32_t raw_payload[], int& words_per_channel, int& samples_per_channel, bool debug);
int Payload_Dump(const uint32_t raw_payload[], bool debug=false );
int Payload_Parse(const uint32_t raw_payload[], std::vector< std::vector< unsigned int >>& data_parsed, bool debug=false);

int Data_Dump( std::unique_ptr<EventFragment>& data, unsigned int n_dump, bool debug=false );
int Data_Write( std::unique_ptr<EventFragment>& data, std::string outputpath, DumpMode mode = DumpMode::New, bool debug=false );

#endif



