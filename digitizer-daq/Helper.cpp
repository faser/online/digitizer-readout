/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper.cpp
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper utilities for code
 *
 */
 
#include "Helper.h"
#include <cstring>
#include <getopt.h>
#include <bitset>
// #include "EventFormats/DAQFormats.hpp"
#include <regex> 

using std::cout, std::endl, std::vector;

/*!
 * \brief Load a json file
 *
 * Opens a text file that contains a json object and loads it into memory
 *
 * \param[in] [filepath] Path to the text file containing the json object
 * \return json object that can be navigated using normal calls to the nlohmann json library
 *
 */
json openJsonFile(const std::string &filepath){
  INFO("Opening config : "<<filepath);
  std::ifstream file(filepath);
  if (!file) {
    throw std::runtime_error("Could not open json config file : "+filepath);
  }
  json j;
  try {
    j = json::parse(file);
  }catch (json::parse_error &e) {
    throw std::runtime_error(e.what());
  }
  file.close();
  return j;
}

/*!
 * \brief Convert word to string
 *
 * Converts a 32 bit word into a human interpretable binary format so that it is easier to print and determine the value of a specific bit
 *
 * \param[in] [word] The 32 bit word to convert
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * \return String formatted as eight sets of four bit sequences from bit[31,0]
 *
 */
std::string ConvertIntToWord(unsigned int word, bool debug){
  // converts an unsigned int to a string of 0's and 1's formatted
  // in an appropriate manner as a 32 bit word

  std::string wordout;

  for (int iBit=31; iBit>=0; iBit--){

    unsigned int bitval = (word & (1<<iBit)) >> iBit;

    if (debug) INFO("bitval : "<<std::dec<<iBit<<"  "<<bitval);

    wordout += std::to_string(bitval);

    if (iBit % 4 == 0) wordout += " ";
  }
  if (debug) INFO("wordout : " << wordout);
  return wordout;
}

/*!
 * \brief Convert single bit in word
 *
 * Sets the value of a single bit in a word to a specific value
 *
 * \param[out] [word] The word that is to be manipulated
 * \param[in]  [bit_location] The bit location that you want to manipulate
 * \param[in]  [bit_val] The intended value of that bit - {0,1}
 *
 * \todo Don't have the function exit, but perhaps print a warning or at worst throw an exception
 */
void SetWordBit(unsigned int& word, int bit_location, unsigned int bit_val){
  if (bit_val==0){
      // n=3
      // prev      = 01101011 
      // mask      = 11110111 [~(1<<3)]
      // prev&mask = 01100011 [only bit 3 has changed]
      word &= ~( 1u << bit_location );
  }
  else if(bit_val==1){
      // n=4
      // prev      = 01101011 
      // mask      = 00010000 [(1<<4)]
      // prev|mask = 01111011 [only bit 4 has changed]
      word |=  ( 1u << bit_location );
  }
  else{
      INFO("This is not a valid bit value to set - exitting");
      exit(18);
  }

}

/*!
 * \brief Decode bit of word
 *
 * Decode the value of a particular bit or a single word
 *
 * \param[in]  [word] The word that is to be decoded
 * \param[in]  [bit_location] The bit location that you want to manipulate
 *
 */
unsigned int GetWordBit(unsigned int word, int bit_location){
  // obtain the value of a particular bit in a word

  word =  (word>>bit_location);
  word &= 0x1;
  
  return word;
}

/*!
 * \brief Get current date
 *
 * Gets the date, in the format of <DateYYYYMMDD_TimeHHMMSS> and returns this as a string.  This can be useful if you want to put a timestamp on someting, like an output text file.
 *
 * \return A string which is the current time, formatted as <DateYYYYMMDD_TimeHHMMSS>
 *
 */
std::string GetDateVerboseString(){
  auto now = std::chrono::system_clock::now();
  auto in_time_t = std::chrono::system_clock::to_time_t(now);

  std::stringstream ss;
  ss << std::put_time(std::localtime(&in_time_t), "Date%Y%m%d_Time%H%M%S");
  return ss.str();
}

/*!
 * \brief Pause running of process
 *
 * Pause the running of the current process for an amount of time specified in [s]
 * 
 * \param[in] [nseconds] Duration of pause in [s]
 *
 */
void Wait(float nseconds){
    DEBUG("Waiting for : "<<nseconds<<" seconds");
    useconds_t nmicroseconds = floor(nseconds*1000000.0);
    usleep(nmicroseconds);
}


/*!
 * \brief Is the string an IP address
 *
 * Determine if the string takes the appropriate format of an IP address.  In FASER, this is used at configuration so that you need not always provide the cryptic IP address assigned by DHCP to the interface board, but rather the verbose human identifiable name(e.g. faser-vme-controller00)
 *
 * \param[in] [ipString] Input string to verify
 * \return Boolean determination of whether the string is an IP address
 *
 */
bool is_ip_address(const std::string &ipString) {
    // regex pattern matching IPv4 address
    std::regex self_regex("^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\\.(?!$)|$)){4}$", std::regex_constants::ECMAScript | std::regex_constants::icase);
    return std::regex_search(ipString, self_regex);
}

// gets the IP address given either a hardcoded IP address of the desired hostname
/*!
 * \brief Find IP address
 *
 * Searches for the IP address of the given computer name.  In FASER, this is used at configuration so that you need not always provide the cryptic IP address assigned by DHCP to the interface board, but rather the verbose human identifiable name(e.g. faser-vme-controller00)
 *
 * \param[in] [ipString] The input compute name to search for.
 * \return The string which is the IP address found
 */
std::string get_ip_address(const std::string &ipString) {
    if (is_ip_address(ipString)) {
        INFO("Interpreting as direct IP address");
        return ipString;
    }

    INFO("Interpreting as hostname and translating to IP");
    struct hostent *he;
    // struct in_addr **addr_list;

    he = gethostbyname(ipString.c_str());
    if (he == NULL) {
        // ERROR("Could not resolve hostname to IP address");
        throw std::runtime_error("Could not resolve hostname to IP address");
    }
    cout << "IP address unic: " << inet_ntoa(*(struct in_addr *)he->h_addr) << endl;
    // char ip_addr_string[32];
    // int i = 0;
    // while ( he -> h_addr_list[i] != NULL) {
    //   strcpy(ip_addr_string, inet_ntoa( *( struct in_addr*)( he -> h_addr_list[i])));
    //   INFO( "Found IP address : "<< inet_ntoa( *( struct in_addr*)( he -> h_addr_list[i])));
    //   i++;
    // }
    std::string returnedIP = inet_ntoa(*(struct in_addr *)he->h_addr);
    return returnedIP;
}

/**
 * @brief Checks if the given rotary format is correct.
 * @details The rotary format is 0x followed by 8 numbers from 0 to 9 which corresponds to the following regular expression :  ^0x[0-9]$
 * This function takes a string representing a rotary format and checks if it is in the correct format.
 *
 * @param rotary The rotary format to be checked.
 * @return True if the rotary format is correct, false otherwise.
 */
bool correct_rotary_format(const std::string &rotary) {
    std::regex self_regex("0x[0-9]{8}", std::regex_constants::ECMAScript);
    return std::regex_search(rotary, self_regex);
}