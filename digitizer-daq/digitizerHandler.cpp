/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

#include "digitizerHandler.h"

#include <chrono>

using json = nlohmann::json;

/**
 * @brief Constructor for digitizerHandler class.
 * @details This class acts as a container for the digitizers. It passes the commands to all digitizers.
 * If the configuration file does not contain the "digitizers" key (format used before adding second digitizer)
 * the constructor will first convert the old format to the new format.
 * @param ip The IP address of the digitizer.
 * @param config The configuration JSON object.
 * @return digitizerHandler object.
 * @see vx1730 class
 */
digitizerHandler::digitizerHandler(char ip[], json config) {
    m_config = config;
    int noDigitizer = 0;

    if (!m_config.contains("digitizers")) {
        INFO("Converting from old configuration format to new format");
        m_config["digitizers"] = json::array();
        json d = {
            {"digitizer_hw_address", config["digitizer_hw_address"].get<std::string>()},
            {"buffer", config["buffer"]},
            {"trigger_internal", config["trigger_internal"]},
            {"trigger_internal_polarity", config["trigger_internal_polarity"].get<std::string>()},
            {"channel_readout", config["channel_readout"]}};
        m_config["digitizers"].push_back(d);
    }

    m_nbDigitizers = m_config["digitizers"].size();
    INFO("Number of digitizers specified in the config : " << m_nbDigitizers);
    const int initial_n_busy_level = m_config["readout"]["n_busy_level"].get<int>();
    const int blt_size = m_config["readout"]["readout_blt"].get<int>();
    const auto& readout = m_config["readout"];
    const auto& parsing = m_config["parsing"];
    const auto& trigger_acquisition = m_config["trigger_acquisition"];
    const auto& trigger_software_rate = m_config["trigger_software_rate"];
    for (auto& c : m_config["digitizers"]) {
        c["readout"] = readout;
        // because reading out the data from the digitizer takes time, it is possible that when reading the first digitizer while almost full, the limit is reached for the next digitizers.
        // To prevent this, each digitizers has a limit raised at n * readout_blt, where n is the order at which the digitizer is read.
        // NOTE : n_busy_level = n * readout_blt should not exceed 1024, which is the max number of event a digitizer can hold.
        if (initial_n_busy_level + noDigitizer * blt_size >= 1024) {
            WARNING("<n_busy_level> is exceeding the max amout of event (1024), currently " << initial_n_busy_level + noDigitizer * blt_size);
        }
        c["readout"]["n_busy_level"] = initial_n_busy_level + noDigitizer * blt_size;
        c["parsing"] = parsing;
        c["trigger_acquisition"] = trigger_acquisition;
        c["trigger_software_rate"] = trigger_software_rate;

        INFO("Getting VME base address: " << c["digitizer_hw_address"]);

        unsigned int vme_base_address;

        if (c.contains("digitizer_hw_address") && correct_rotary_format(c["digitizer_hw_address"].get<string>())) {
            vme_base_address = std::stoul(c["digitizer_hw_address"].get<std::string>(), 0, 16);
        } else {
            throw std::runtime_error("No valid VME Base address setting in the digitizer configuration.");
        }
        m_digitizers.push_back(std::move(std::make_unique<vx1730>(ip, vme_base_address)));
        noDigitizer++;
    }
}

/**
 * @brief Configure the ethernet transmission of the digitizers.
 * @return void
 * @see vx1730::SetInterfaceEthernetJumboFrames()
 * @see vx1730::SetInterfaceEthernetGap()
 * @see vx1730::SetInterfaceEthernetMaxPackets()
 */
void digitizerHandler::configure_ethernet_transmission() {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        json& config = m_config["digitizers"][i];
        bool interfaceJumbo = config["readout"]["interface_jumbo"].get<bool>();
        UINT interfacePacketGap = config["readout"]["interface_packet_gap"].get<UINT>();
        UINT interfaceMaxPackets = config["readout"]["interface_max_packets"].get<UINT>();

        m_digitizers[i]->SetInterfaceEthernetJumboFrames(interfaceJumbo);
        m_digitizers[i]->SetInterfaceEthernetGap(interfacePacketGap);
        m_digitizers[i]->SetInterfaceEthernetMaxPackets(interfaceMaxPackets);
    }
}

/**
 * @brief Perform an interface speed test.
 * @param nreads The number of reads to perform.
 * @return void
 * @see vx1730::PerformInterfaceSpeedTest()
 */
void digitizerHandler::perform_interface_speedtest(unsigned int nreads) {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "] ");
        float interface_rate = m_digitizers[i]->PerformInterfaceSpeedTest(nreads);
        INFO("Speed(interface) [words/s]: " << interface_rate);
        INFO("Speed(interface) [MB/s]: " << (interface_rate * 4) / 1000000.);
    }
}

/**
 * @brief Test the communication with the crate controller and the digitizers.
 * @return void
 * @see vx1730::TestCommInterface()
 * @see vx1730::TestCommDigitizer()
 */
void digitizerHandler::test_communication() {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "] ");
        m_digitizers[i]->TestCommInterface();
        m_digitizers[i]->TestCommDigitizer(m_config["digitizers"][i]);
    }
}
/**
 * @brief Dump the configuration of the digitizers and print it on stdout.
 * @return void
 * @see vx1730::DumpConfig()
 */
void digitizerHandler::dump_config() {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "] ");
        m_digitizers[i]->DumpConfig();
    }
}

/**
 * @brief Configure the digitizers.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::Configure()
 */
void digitizerHandler::configure(bool debug) {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "] ");
        m_digitizers[i]->Configure(m_config["digitizers"][i], debug);
    }
}

/**
 * @brief Start the acquisition of the digitizers.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::StartAcquisition()
 */
void digitizerHandler::start_acquisition(bool debug) {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "] ");
        m_digitizers[i]->StartAcquisition(debug);
    }
}

/**
 * @brief Send a software trigger to the digitizers.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::SendSWTrigger()
 * @note If called multiple times, should wait a bit between each call (0.002s for example)
 */
void digitizerHandler::send_sw_trigger(bool debug) {
    for (auto& d : m_digitizers) {
        d->SendSWTrigger(debug);
    }
}

/**
 * @brief Stop the acquisition of the digitizers.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::StopAcquisition()
 */
void digitizerHandler::stop_acquisition(bool debug) {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        INFO("Digitizer [" << i << "]");
        m_digitizers[i]->StopAcquisition(debug);
    }
}

/**
 * @brief Get the event sizes of the digitizers.
 * @return std::vector<int> The event sizes of the digitizers.
 * @see vx1730::RetrieveBufferLength()
 */
std::vector<size_t> digitizerHandler::get_event_sizes() {
    std::vector<size_t> eventSizes(m_nbDigitizers);
    unsigned int bufferLength;
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        auto& c = m_config["digitizers"][i];
        bufferLength = m_digitizers[i]->RetrieveBufferLength();
        unsigned int nchannels_enabled = 0;
        const auto& channel_readout = c["channel_readout"];
        for (uint8_t iChan = 0; iChan < 16; iChan++) {
            if (channel_readout[iChan]["enable"].get<int>() == 1)
                nchannels_enabled++;
        }
        eventSizes[i] = ((bufferLength / 2) * nchannels_enabled) + 4;
    }
    return eventSizes;
}

/**
 * @brief Dump the event count of the digitizers.
 * @param debug If true, print debug messages.
 * @return int The event count of the digitizers.
 * @see vx1730::DumpEventCount()
 * @note Currently used in the DigitizerReceiverModule.
 */
size_t digitizerHandler::dump_event_count(bool debug, size_t d_id) {
    return m_digitizers[d_id]->DumpEventCount(debug);
}

/**
 * @brief Dump the event count of the digitizers.
 * @param occupancies The event count of the digitizers.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::DumpEventCount()
 * @note Currently used in the standalone script. Could be used in the DigitizerReceiverModule.
 */
void digitizerHandler::dump_event_count(std::vector<UINT>& occupancies, bool debug) {
    for (size_t i = 0; i < m_nbDigitizers; i++) {
        occupancies[i] = m_digitizers[i]->DumpEventCount(debug);
    }
}

/**
 * @brief Read a single event from the digitizers.
 * @param d_id The digitizer ID (index in the m_digitizers vector).
 * @param readout The readout object.
 * @param n_events_to_do The number of events to read.
 * @param debug If true, print debug messages.
 * @return void
 * @see vx1730::ReadSingleEvent()
 */
void digitizerHandler::read_single_event(size_t d_id, READOUT& readout, unsigned int n_events_to_do, bool debug) {
    readout.nerrors = 0;
    // clear the monitoring map which will retrieve the info to pass to monitoring metrics
    readout.monitoring.clear();
    readout.nwordsObtained = m_digitizers[d_id]->ReadSingleEvent(readout.raw_payload, readout.eventSize * n_events_to_do, readout.monitoring, readout.nerrors, m_config["readout"]["readout_method"], debug);
}
