/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper.h
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper utilities for code
 *
 * These are additional utilities, not necessarily specific to the digitizer, which are used
 * throughout.  For example, there are utilities that cast words into more interpretable strings.
 */

#ifndef HELPER_INCLUDE_H
#define HELPER_INCLUDE_H

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>

#include <atomic>
#include <chrono>
#include <cmath>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <type_traits>

#include "nlohmann/json.hpp"
#include "project_interface_define.h"  //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "project_system_define.h"     //define LINUX or WINDOWS
#include "sis3153ETH_vme_class.h"
#include "sis3153usb.h"
#include "vme_interface_class.h"
// for convenience
using json = nlohmann::json;
using namespace std::chrono;

// only needed for standalone compilation, otherwise you want to use the real logging in daqling
#include "Logging.hpp"

// for exception construction
#include "Exceptions/Exceptions.hpp"

/*!
 * \class DigitizerHardwareException
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 January 2021
 *
 * \brief To allow for digitizer specific exception handline
 *
 */
class DigitizerHardwareException : public Exceptions::BaseException {
    using Exceptions::BaseException::BaseException;
};

json openJsonFile(const std::string &filepath);

std::string ConvertIntToWord(unsigned int word, bool debug = false);

void SetWordBit(unsigned int &word, int bit_location, unsigned int bit_val);

unsigned int GetWordBit(unsigned int word, int bit_location);

std::string GetDateVerboseString();

void Wait(float nseconds);


struct MonitoringValuesHandler {
    MonitoringValuesHandler() : temps(16) {}
    // registred for grafana
    std::atomic<int> pedestal[16];
    std::vector<std::atomic<int>> temps;
    std::map<std::string, float> monitoring;

    std::atomic<int> hw_buffer_space;
    std::atomic<int> hw_buffer_occupancy;

    std::atomic<int> triggers;  // number of triggers
    std::atomic<float> time_read;
    std::atomic<float> time_parse;
    std::atomic<float> time_overhead;
    std::atomic<int> corrupted_events;
    std::atomic<int> empty_events;

    // intermediate monitoring variables, reset after each read sequence (once n_events_present == 0)
    float intermediate_read_time = 0;
    size_t intermediate_receivedEvents = 0;
    float intermediate_parse_time = 0;
};

struct READOUT {
    unsigned int nerrors = 0;
    size_t eventSize;
    size_t nwordsObtained = 0;
    std::map<std::string, float> monitoring;
    UINT *raw_payload;
    ~READOUT() {
        delete[] raw_payload;
        raw_payload = nullptr;
    }
};

bool is_ip_address(const std::string &ipString);
std::string get_ip_address(const std::string &ipString);
bool correct_rotary_format(const std::string &rotary);

/*!
 * \brief Convert hexidecimal to string
 *
 * Converts a hexidecimal number to a string
 *
 * \param[in] [input] Input hexidecimal number to be converted (int or unsigned int)
 * \return The string representation of the hexidecimal value
 * 
 */
template <typename T>
std::string ConvertDecToHexString(T input){
  static_assert(std::is_same<T, int>::value || std::is_same<T, unsigned int>::value, 
                "Invalid type for ConvertDecToHexString: only int and unsigned int are allowed");

  std::stringstream ss;
  ss << std::hex << input;
  return ss.str();
}


#endif
