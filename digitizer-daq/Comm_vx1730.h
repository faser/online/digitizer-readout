/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Comm_vx1730.h
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Registers for the interface board
 *
 */

#ifndef  COMM_VX1730_INCLUDE_H
#define  COMM_VX1730_INCLUDE_H

#include "project_system_define.h"      //define LINUX or WINDOWS
#include "project_interface_define.h"   //define Interface (sis1100/sis310x, sis3150usb or Ethnernet UDP)
#include "vme_interface_class.h"
#include "sis3153usb.h"
#include "sis3153ETH_vme_class.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <cmath>
#include <ctime>
#include "Registers_vx1730.h"

#include "Helper.h"
#include "Helper_Event.h"
#include "Helper_sis3153.h"

#include "nlohmann/json.hpp"
using json = nlohmann::json;

#include <chrono>
using namespace std::chrono;

/*!
 * \class vx1730
 *
 * \author Sam Meehan
 * \date 13 January 2021
 *
 * \brief To be used to access the vx1730 digitizer via the sis3153 interface board with ethernet interface.
 *
 * Creates an instance of a single digitizer identified by the ip address and VME base address.
 * It is necessary to read about the CAEN digitizer board to understand how to configure the 
 * VME base address via the rotary switches.  You are referred to find this documentation on
 * <a href="https://www.caen.it/products/vx1730/">the CAEN website</a>.
 */
class vx1730{
  public:

    //////////////////////////////////////
    // Helper functions
    //////////////////////////////////////
    vx1730(char ip[], unsigned int vme_base);
    ~vx1730(); 
    //////////////////////////////////////
    // Basic information for interface board
    //////////////////////////////////////
    /*!
     * \brief Object for accessing the board
     *
     * Primary object that utilizes the core read/write functionality provided in the CAEN software
     */
    sis3153eth* m_crate;
    /*!
     * \brief Base address of digitizer in VME crate
     *
     * This base address is set via the physical rotary switches on the digitizer board and used
     * as the first four entries to specify the location of a register on this board
     * 
     * RegisterAddress : 0xBBBBAAAA
     * 
     * where BBBB correspond to the rotary switches and is the base address here, and AAAA are the 
     * register specific addresses specified in Registers_vx1730.h
     */
    unsigned int m_base_address; // this is the base address of the digitizer - configured physically on the board

    //////////////////////////////////////
    // Monitoring information
    //////////////////////////////////////
    void TestCommInterface(bool debug=false);
    void TestCommDigitizer(const json& config);
    void DumpConfig();
    void MonitorTemperature( std::string outputfile, bool debug=false);
    std::vector<int> GetADCTemperature(bool debug);
    
    //////////////////////////////////////
    // Configuration of interface board
    //////////////////////////////////////
    void SetInterfaceEthernetJumboFrames(bool toggle=true);
    void SetInterfaceEthernetMaxPackets(unsigned int n_max_packets);
    void SetInterfaceEthernetGap(unsigned int gap_setting);
    
    //////////////////////////////////////
    // Speed tests
    //////////////////////////////////////
    float PerformInterfaceSpeedTest(unsigned int nreads=100);
    float PerformInterfaceVMESpeedTest(int nreads=100); 
    
    //////////////////////////////////////
    // Board control
    //////////////////////////////////////
    void Configure(json config, bool debug=false);
    void Reset(bool debug=false);
    void StartAcquisition( bool debug=false );
    void StopAcquisition( bool debug=false );
    void ADCCalibration( bool debug=false );
  
    //////////////////////////////////////
    // Event readout 
    //////////////////////////////////////
    // for standalone usage
    void DumpFrontEvent( std::string outputfile, DumpMode mode = DumpMode::New, bool debug=false);
    
    // single event like in DAQling
    void SendEventSingle(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug=false);
    void RetrieveEventSingle(uint32_t raw_payload[], int software_buffer, std::map<std::string, float>& monitoring, int nevents, int nchannels_enabled, int buffer_size, std::string readout_method, int events_to_readout, int ECRcount, float ttt_converter, bool debug=false);
    int  ReadEventSingle( uint32_t raw_payload[], bool debug=false );
    std::unique_ptr<EventFragment> ParseEventSingle(uint32_t raw_payload[], unsigned int event_size, 
							std::map<std::string, float>& monitoring, 
						        int ECRcount, float ttt_converter, float bcid_ttt_fix, 
							unsigned int errors,
							bool debug=false, size_t d_id = 0);

    unsigned int ReadSingleEvent( uint32_t raw_payload[], unsigned int event_size, std::map<std::string, float>& monitoring, unsigned int& errors, 
			 const std::string& readout_method, bool debug=false );
    // helper function for parsing multiple event read
    int GetSingleEvent( uint32_t raw_payload[], uint32_t single_event_raw_payload[], int eventLocation, int eventSize);

    //////////////////////////////////////
    // Helper functions
    //////////////////////////////////////
    void SendSWTrigger( bool debug=false );
    unsigned int DumpEventCount( bool debug=false );
    unsigned RetrieveBufferLength();
    unsigned int GetBufferLength( unsigned int code );

    unsigned int GetEventSize(bool debug = false);
    int m_pedestal[16];
    unsigned int m_minmax[16];

  protected:
  
    // individual methods used within the configuration
    void ConfigReset(bool debug=false);
    
    void ConfigBuffer(const json& config, bool debug=false);
    void ConfigReadoutEnable(const json& config, bool debug=false);
    void ConfigVoltageReading(const json& config, bool debug=false);
    void ConfigBlockReadout(const json& config, bool debug=false);
    void ConfigInternalSawtooth(const json& config, bool debug=false);
    
    void ConfigEventReadoutTrigger(const json& config, bool debug=false);  
    void ConfigGroupTriggerSettings(const json& config, bool debug=false); 
    void ConfigTriggerCoincidence(const json& config, bool debug=false);
    void ConfigFrontPanelTrigger(bool debug=false);

    void ConfigMeasurePedestals(const json& config, bool debug=false);  

    void ConfigLVDS(const json& config, bool debug=false);
    
    // for setting individual registers for specific actions
    void SetTriggerChannelThreshold(unsigned int channel, unsigned int threshold);
    void SetTriggerGroupOutputWidth(unsigned int group, unsigned int width);
    void SetChannelDynamicRange(unsigned int channel, double dynamic_range);
    void SetChannelDCOffset(unsigned int channel, double dc_offset);
};

#endif

