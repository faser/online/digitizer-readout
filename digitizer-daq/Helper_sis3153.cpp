/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file Helper_sis3153.cpp
 * \copyright Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Helper methods for facilitating communication via ethernet with the system
 *
 * Core methods for communicating with the VME crate.  These allow for communication
 * with the control board and any daughter boards in the crate as well.  These boards are
 * commonly referred to as "master" and "slave" and these are used throughout this library, but
 * it should be noted that there is an initiative to phase out this terminology in electronics
 * - https://sites.google.com/view/mspledge - and in the future, hopefully this code can be made
 * to adhere to that plegde.
 */

#include "Helper_sis3153.h"

/*!
 * \brief Read from Control
 *
 * Perform a single of a 32 bit word of a register at a specific address in the control interface board.
 *
 * \param[in] [crate] The interface board object in which the SIS methods are implemented
 * \param[in] [addr]  The 32 bit address of the interface board
 * \param[out] [data] The location where the 32 bit word that is being read will be stored in software
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 */
void ReadMasterReg( sis3153eth* crate, unsigned int addr, unsigned int& data, [[maybe_unused]] bool debug){
  int return_code;
  unsigned int data_read; // temporary variable that is then copied to the input reference

  return_code = crate->udp_sis3153_register_read (addr, &data_read);
  data = data_read;
  
  if(return_code!=0){
    ERROR("ReadMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);

    if(return_code==0x111){ 
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    }
    else if(return_code==0x211){
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    }
    else if(return_code==0x212){
      ERROR("VME Retry");
    }
    else if(return_code==214){
      ERROR("VME Arbitration Timeout");
    }
    else{
      ERROR("Unknown return code from sis3153 software");
    }
    THROW(DigitizerHardwareException, "Unable to perform ReadMasterReg at addr = 0x"+ConvertDecToHexString(addr)+" with return_code = 0x"+ConvertDecToHexString(return_code));
  }
}

/*!
 * \brief Write to Control
 *
 * Perform a write of a 32 bit word of a register at a specific address in the control interface board.
 *
 * \param[in] [crate] The interface board object in which the SIS methods are implemented
 * \param[in] [addr]  The 32 bit address of the interface board
 * \param[out] [data] The location where the 32 bit word that is to be written to the location addr
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 */
void WriteMasterReg( sis3153eth* crate, unsigned int addr, unsigned int data, [[maybe_unused]] bool debug){
  int return_code;
  return_code = crate->udp_sis3153_register_write (addr, data);

  if(return_code!=0){
    ERROR("WriteMasterReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
  
    if(return_code==0x111){
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    }
    else if(return_code==0x211){
      ERROR("VME Bus Error - Is the board fully plugged in. Remember, the VME crate in b161 is finicky.");
    }
    else if(return_code==0x212){
      ERROR("VME Retry");
    }
    else if(return_code==0x214){
      ERROR("MVE Arbitration Timeout");
    }
    else{  
      ERROR("Unknown return code");
    }
    THROW(DigitizerHardwareException, "Unable to perform WriteMasterReg addr = 0x"+ConvertDecToHexString(addr)+" with return_code = 0x"+ConvertDecToHexString(return_code));
  }
}

/*!
 * \brief Read from Daughter
 *
 * Perform a read of a 32 bit word of a register at a specific address in the digitizer board
 *
 * \param[in] [crate] The interface board object in which the SIS methods are implemented
 * \param[in] [addr]  The 32 bit address of the register on the digitizer board, composed of the m_base_address and the specific register address specified in Registers_vx1730.h
 * \param[out] [data] The location where the 32 bit word that is being read will be stored in software
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 */
void ReadSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int& data, [[maybe_unused]] bool debug){
  int return_code;
  unsigned int data_read;

  return_code = crate->vme_A32D32_read (addr, &data_read);
  data = data_read;
  
  if(return_code!=0){
    ERROR("ReadSlaveReg :  addr = "<<std::hex<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
    
    if(return_code==0x111){
      ERROR("PROTOCOL_ERROR_CODE_TIMEOUT");
    }
    else if(return_code==0x121){
      ERROR("PROTOCOL_ERROR_CODE_WRONG_NOF_RECEVEID_BYTES : wrong Packet Length after N Retransmit and M Request commands");
    }
    else if(return_code==0x122){
      ERROR("PROTOCOL_ERROR_CODE_WRONG_PACKET_IDENTIFIER : wrong received Packet ID after N Retransmit and M Request commands");
    }
    else if(return_code==0x211){
      ERROR("PROTOCOL_VME_CODE_BUS_ERROR");
    }
    else{
      ERROR("Unknown return code : "<<return_code);
    }
    
    THROW(DigitizerHardwareException, "Unable to perform ReadSlaveReg at addr = "+std::to_string(addr)+"    return_code = "+std::to_string(return_code));
  }
}

/*!
 * \brief Write to Daughter
 *
 * Perform a write of a 32 bit word of a register at a specific address in the digitizer board
 *
 * \param[in] [crate] The interface board object in which the SIS methods are implemented
 * \param[in] [addr]  The 32 bit address of the register on the digitizer board, composed of the m_base_address and the specific register address specified in Registers_vx1730.h
 * \param[out] [data] The location where the 32 bit word that is being written to addr
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 *
 */
void WriteSlaveReg( sis3153eth* crate, unsigned int addr, unsigned int data, [[maybe_unused]] bool debug){
  int return_code;
  return_code = crate->vme_A32D32_write (addr, data);

  if(return_code!=0){
    ERROR("WriteSlaveReg :  addr = "<<addr<<"    data = "<<data<<"    return_code = "<<return_code);
    
    if(return_code==0x111){
      ERROR("No Ethernet Connection - Check the IP address, or plugging or ethernet cables.");
    }
    else if(return_code==0x122){
      ERROR("PROTOCOL_ERROR_CODE_WRONG_PACKET_IDENTIFIER : wrong received Packet ID after N Retransmit and M Request command");
    }
    else if(return_code==0x211){
      ERROR("PROTOCOL_VME_CODE_BUS_ERROR");
    }
    else{
      ERROR("Unknown return code");
    }
    THROW(DigitizerHardwareException, "Unable to perform WriteSlaveReg at addr = 0x"+ConvertDecToHexString(addr)+" with return_code = 0x"+ConvertDecToHexString(return_code));
  }
}

/*!
 * \brief Verify communication with interface
 *
 * Perform some basic read/writes to verify the communication with the interface board.
 * One of these is a read/write that will turn on and off the LED on the front of the sis3153 board
 * thereby allowing the user to also visually inspect the functionality, in addition to the return code
 * of the read/write codes themselves.
 *
 * \param[in] [crate] The interface board object in which the SIS methods are implemented
 * \param[in] [debug] A flag that will optionally execute portions of the method for debugging purposes.  This merely prints more debug statements.
 * 
 * \todo More elegantly fold in the feature that toggles the ability to perform a board reset.  Should this go in the configuration?
 */
int sis3153_TestComm( sis3153eth* crate, bool debug ){
  INFO("sis3153_TestComm");

  [[maybe_unused]] int return_code;
  UINT data;
  //remove anything old in buffer
  crate->clear_UdpReceiveBuffer();
  int errorCnt=0;
  while(errorCnt<30) {
    try {
      INFO("Trying to see if we can read master reg");
      ReadMasterReg(crate, SIS3153_CONTROL_STATUS, data, debug);
      break;
    } catch (DigitizerHardwareException &e) {
      errorCnt+=1;
    }
  }
  if (errorCnt)
    WARNING("Got "<<errorCnt<<" errors when trying to read status");
  // read the basic configuration information of the interface board
  ReadMasterReg(crate, SIS3153_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_MODID_VERSION, data, debug);
  ReadMasterReg(crate, SIS3153_SERIAL_NUMBER_REG, data, debug);
  ReadMasterReg(crate, SIS3153_LEMO_IO_CTRL_REG, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_MASTER_CYCLE_STATUS, data, debug);
  ReadMasterReg(crate, SIS3153_VME_INTERRUPT_STATUS, data, debug);
  
  
  while(false){
    auto start = chrono::high_resolution_clock::now(); 
    // speed test
    uint32_t lots_of_data[0x100000];
    uint32_t n_lots_of_words;

    
    crate->udp_sis3153_register_dma_read (0x001, lots_of_data, 0xFFFFF, &n_lots_of_words );
  
    INFO("SIS3153Ethernet Speed : "<<n_lots_of_words);
  
    auto end = chrono::high_resolution_clock::now(); 

    float time_taken = chrono::duration_cast<chrono::nanoseconds>(end - start).count() * 1e-9; 
    DEBUG("Time taken by buffer_readout is : " << fixed << time_taken << setprecision(5) << " sec ");
  }

  // turn the LED A on and off to demonstrate functionality of communication
  DEBUG("=====================================");
  DEBUG("Testing interface card communications with LED A on and off");
  DEBUG("=====================================");
  DEBUG("Turn LED-A on");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  usleep(100000);
  
  DEBUG("Turn LED-A off");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x10000, debug);
  usleep(100000);
  
  DEBUG("Turn LED-A on");
  
  WriteMasterReg(crate, SIS3153_CONTROL_STATUS, 0x1, debug);
  
  DEBUG("The light should still be on for LED A on the interface card");

  // this block is only present to toggle the system reset bit
  if(false){
    // if Switch SW162-7 is ON and VME_SYSRESET bit is set then VME_SYSRESET is issued
    INFO("SysReset");
  
    UINT data_sys_reset;

    ReadMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data_sys_reset, debug);

    DEBUG("Data System Reset Initial : "<<ConvertIntToWord(data_sys_reset));

    SetWordBit(data_sys_reset, 16, 0);
    SetWordBit(data_sys_reset, 0, 1);

    DEBUG("Data System Reset Initial : "<<ConvertIntToWord(data_sys_reset));

    //WriteMasterReg(crate, SIS3153_VME_MASTER_CONTROL_STATUS, data_sys_reset, debug);
  }

  return 0;
}


