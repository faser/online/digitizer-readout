/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

#include "BOBR.h"
sis3153eth *gl_vme_crate;

vme_interface_class *intf;

int main(int argc, char *argv[])
{

	[[maybe_unused]] unsigned int addr;
	[[maybe_unused]] unsigned int data ;
	[[maybe_unused]] unsigned short ushort_data ;
	unsigned char cdata;
	cout << "BOBR access test" << endl;

	   //char char_command[256];
	char  ip_addr_string[32] ;
	[[maybe_unused]] char ch_string[64] ;
	[[maybe_unused]] int int_ch ;
	int return_code ;

	[[maybe_unused]] char  pc_ip_addr_string[32] ;

	// default
	[[maybe_unused]] unsigned int vme_base_address = 0x00B00000 ;
	strcpy(ip_addr_string,"10.11.65.39") ; // SIS3153 IP address

	int reg=0;
	bool write=false;
	int write_value=0;
	if (argc>1) {
	  reg=std::atoi(argv[1]);
	}
	if (argc>2) {
	  write=true;
	  write_value=std::atoi(argv[2]);
	}
	printf("\n");


	sis3153eth *vme_crate;
	sis3153eth(&vme_crate, ip_addr_string);

	char char_messages[128] ;
	unsigned int nof_found_devices ;

	// open Vme Interface device
	return_code = vme_crate->vmeopen();  // open Vme interface
	vme_crate->get_vmeopen_messages (char_messages, &nof_found_devices);  // open Vme interface

        printf("get_vmeopen_messages = %s , nof_found_devices %d \n",char_messages, nof_found_devices);

	/*
	return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_CONTROL_STATUS, &data); //
	printf("Control Status : \n");
	printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_CONTROL_STATUS, data,return_code);

	return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_MODID_VERSION, &data); //
	printf("ModuleID and Firmware Version : \n");
 	printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_MODID_VERSION, data,return_code);

	return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_SERIAL_NUMBER_REG, &data); //
	printf("Serial Number : \n");
	printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_SERIAL_NUMBER_REG, data,return_code);

	return_code = vme_crate->udp_sis3153_register_read (SIS3153USB_LEMO_IO_CTRL_REG, &data); //
	printf("Lemo I/O Register : \n");
	printf("udp_sis3153_register_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", SIS3153USB_LEMO_IO_CTRL_REG, data,return_code);

	printf("\n\n");
	
	printf("Read BOBR data\n\n");

	printf("Firmware version: \n");
	addr=vme_base_address+0x00;
	return_code = vme_crate->vme_A24D32_read (addr, &data);
	printf("vme_A24D32_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);

	printf("Status/Control: \n");
	addr=vme_base_address+0x10;
	return_code = vme_crate->vme_A24D32_read (addr, &data);
	printf("vme_A24D32_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);

	printf("Turn clock count: \n");
	addr=vme_base_address+0x50;
	return_code = vme_crate->vme_A24D32_read (addr, &data);
	printf("vme_A24D32_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);
	data=0x02;
	return_code = vme_crate->vme_A24D32_write (addr, data);
	printf("vme_A24D32_write: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);
	return_code = vme_crate->vme_A24D32_read (addr, &data);
	printf("vme_A24D32_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);


	printf("Status/Control: \n");
	addr=vme_base_address+0x10;
	return_code = vme_crate->vme_A24D32_read (addr, &data);
	printf("vme_A24D32_read: addr = 0x%08X    data = 0x%08X    return_code = 0x%08X \n", addr, data,return_code);
	
	printf("\n\n\n timing data\n");

	// block 0 reading

	return_code = bobr->readBlock0Register (0x1, &cdata);

	return_code = bobr->readBlock0Register (0x5, &cdata);

	return_code = bobr->readBlock0Register (0x11, &cdata);

	return_code = bobr->readBlock0Register (0x13, &cdata);

	printf("\nRead S0\n");
	cdata=0;
	return_code = bobr->writePFCControl (cdata);
	return_code = bobr->readPFCData (&cdata);

	printf("\nRead S2\n");
	cdata=ES1_MASK;
	return_code = bobr->writePFCControl (cdata);
	return_code = bobr->readPFCData (&cdata);
	
	printf("\nWrite S2\n");
	cdata=ES1_MASK;
	return_code = bobr->writePFCControl (cdata);
	cdata=0x18;
	return_code = bobr->writePFCData (cdata);
	cdata=0;
	return_code = bobr->readPFCData (&cdata);

	printf("\nRead S3\n");
	cdata=ES2_MASK;
	return_code = bobr->writePFCControl (cdata);
	return_code = bobr->readPFCData (&cdata);

	printf("\nRead S0\n");
	cdata=0;
	return_code = bobr->writePFCControl (cdata);
	return_code = bobr->readPFCData (&cdata);

	printf("\n");
	return_code = bobr->readPFCStatus (&cdata);
	printf("\n");
*/
	printf("Init PFC\n");
	BOBR *bobr = new BOBR(vme_crate,true);
	return_code = bobr->initPFC();
	if (return_code) {
	  printf("Failed to initialize PFC\n");
	  return 1;
	}

	printf("\nRead TTC register %d\n",reg);
	return_code = bobr->readTTCRegister(reg,&cdata);
	if (return_code) {
	  printf("Failed to read TTC register\n");
	  return 1;
	}
	printf("Read TTC value in register %d: 0x%02X   rc=%02X\n",reg,cdata,return_code);
	if (write) {
	  printf("\nWrite 0x%02X to TTC register %d\n",write_value,reg);
	  unsigned int lock_status;
	  bobr->readLockStatus(&lock_status);
	  printf("Current lock status: %d\n",lock_status);
	  return_code=bobr->writeTTCRegister(reg,write_value);
	  if (return_code) {
	    printf("Failed to write to TTC register\n");
	    return 1;
	  }
	  printf("Lock: ");
	  for(int ii=0;ii<100;ii++) {
	    return_code=bobr->readLockStatus(&lock_status);
	    if (return_code) break;
	    printf("%d",lock_status);
	    usleep(1000);
	  }
	  printf("\n");
	  printf("Read back value:\n");
	  return_code = bobr->readTTCRegister(reg,&cdata);
	  if (return_code) {
	    printf("Failed to re-read TTC register\n");
	    return 1;
	  }
	  printf("Read back TTC value in register %d: 0x%02X   rc=%02X\n",reg,cdata,return_code);
	}



	return 0;
}

