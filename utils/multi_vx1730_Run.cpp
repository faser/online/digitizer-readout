/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

/***********************************************
 *  \file multi_vx1730_Run.cpp
 *  \brief Standalone program to test multi digitizer readout
 ***********************************************/

#include <ctime>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

#include "Helper.h"
#include "digitizerHandler.h"

using json = nlohmann::json;
using std::cout, std::string, std::endl;

enum class RunType { ShowConfig,
                     SoftwareTriggerRun,
                     TimedRun,
                     StopRun };


// Parameters of the program (explained in the help menu)
struct Params {
    std::string configPath = "";
    std::string outputPath = "";
    RunType runType = RunType::ShowConfig;
    unsigned int numberSwTriggers = 0;
    unsigned int runTime = 0;
    unsigned int bufferLength = 0;
    unsigned int bltSize = 0;
};

// emulating DAQling monitoring features
struct Monitoring {
    unsigned int hw_buffer_space;
    unsigned int hw_buffer_occupancy;
    unsigned int triggers;
    float time_read;
    float time_parse;
    float time_overhead;
    unsigned int corrupted_events;
    unsigned int empty_events;

    // resetted after each read sequence.
    float intermediate_read_time;
    unsigned int intermediate_receivedEvents = 0;
    float intermediate_parse_time;
};

/**
 * Converts a string representation of a run type to its corresponding RunType enum value.
 *
 * @param s The string representation of the run type.
 * @return The RunType enum value corresponding to the input string.
 */
inline RunType runTypeFromString(const std::string &s) {
    if (s == "ShowConfig") {
        return RunType::ShowConfig;
    } else if (s == "SoftwareTriggerRun") {
        return RunType::SoftwareTriggerRun;
    } else if (s == "TimedRun") {
        return RunType::TimedRun;
    } else if (s == "StopRun") {
        return RunType::StopRun;
    } else {
        cout << "Unknown run type: " << s << "\n";
        cout << "Exiting..." << endl;
        exit(1);
    }
}

/**
 * \brief Writes the raw payload data to a text file.
 *
 * @param outfile The output file stream to write to.
 * @param raw_payload The array containing the raw payload data.
 * @param eventSize The size of the event.
 * @param debug Enable debug mode or not.
 * @return void
 *
 * \warning This function should only be used for testing purposes, since writing to file is very slow and the file takes a lot of space.
 */
void write_to_file(std::ofstream &outfile, uint32_t raw_payload[], int eventSize) {
    for (int i = 0; i < eventSize; i++) {
        outfile << raw_payload[i] << " ";
    }
    outfile << endl;
}

int main(int argc, char *argv[]) {
    int opt;
    Params params;
    while ((opt = getopt(argc, argv, "hc:o:r:n:L:B:t:")) != -1) {
        switch (opt) {
            case 'h':
                cout << "Help menu: \n \n";
                cout << std::left << std::setw(20) << "Option name" << std::setw(30) << "Argument" << "Description" << "\n";
                cout << std::setw(20) << "-----------" << std::setw(30) << "--------" << "----------" << "\n\n";
                cout << std::setw(20) << "-h :" << std::setw(30) << " " << "Prints the help message." << "\n";
                cout << std::setw(20) << "-c :" << std::setw(30) << "[CONFIG_PATH]" << "Path to the config file." << "\n";
                cout << std::setw(20) << "-o :" << std::setw(30) << "[OUTPUT_PATH]" << "Path to the output file. Needed for TimedRun." << "\n";
                cout << std::setw(20) << "-L :" << std::setw(30) << "[BUFFER_LENGTH]" << "Overrides the buffer length specified in the config file." << "\n";
                cout << std::setw(20) << "-B :" << std::setw(30) << "[BLT_SIZE]" << "Overrides the blt size specified in the config file." << "\n";
                cout << std::setw(20) << "-n :" << std::setw(30) << "[NUMBER_SWTRIGGERS]" << "Number of software trigger. Needed for SoftwareTriggerRun." << "\n";
                cout << std::setw(20) << "-t :" << std::setw(30) << "[AMOUT_TIME_SEC]" << "Amount of time in seconds. Needed for TimedRun." << "\n";
                cout << std::setw(20) << "-r :" << std::setw(30) << "[RUN_TYPE]" << "Type of the run :" << "\n";
                cout << std::setw(20) << " " << std::setw(30) << " " << "\t- ShowConfig : writes the config to the digitizers and print it in the stdout." << "\n";
                cout << std::setw(20) << " " << std::setw(30) << " " << "\t- SoftwareTriggerRun : Data acquisition stops after [NUMBER_SWTRIGGERS] are read." << "\n";
                cout << std::setw(20) << " " << std::setw(30) << " " << "\t- TimedRun : Data acquisition stops after [AMOUT_TIME_SEC] time." << "\n";
                cout << std::setw(20) << " " << std::setw(30) << " " << "\t- StopRun : Stops the current Data acquisition." << "\n";
                cout << endl;
                exit(0);
                break;
            case 'c':
                params.configPath = optarg;
                break;
            case 'o':
                params.outputPath = optarg;
                break;
            case 'r':
                params.runType = runTypeFromString(optarg);
                break;
            case 'n':
                params.numberSwTriggers = static_cast<unsigned int>(std::stoi(optarg));
                break;
            case 'L':
                params.bufferLength = static_cast<unsigned int>(std::stoi(optarg));
                break;
            case 'B':
                params.bltSize = static_cast<unsigned int>(std::stoi(optarg));
                break;

            case '?':
                cout << "Unknown option: " << optopt << endl;
                break;
        }
    }

    /// Checking validity of the parameters

    if (params.configPath == "") {
        cout << "No config file specified. Exiting..." << endl;
        exit(1);
    }
    if ((params.runType == RunType::SoftwareTriggerRun) && (params.numberSwTriggers == 0)) {
        cout << "Number of software triggers not specified. Exiting..." << endl;
        exit(1);
    } else if ((params.runType == RunType::TimedRun) && (params.runTime == 0 || params.outputPath == "")) {
        cout << "Run time or outputPath not specified. Exiting..." << endl;
        exit(1);
    }

    // loading the configuration file
    json generalConfig = openJsonFile(params.configPath);

    if (params.bltSize > 0) {
        generalConfig["readout"]["readout_blt"] == params.bltSize;
    }

    if (params.bufferLength > 0) {
        for (auto &c : generalConfig["digitizers"]) {
            if (params.bufferLength % 40 != 0) {
                cout << "Buffer length must be a multiple of 40. Exiting..." << endl;
                exit(1);
            }
            c["buffer"]["length"] = params.bufferLength;
        }
    }

    // converting if needed to ip address format
    std::string input_ip_location;
    try {
        input_ip_location = get_ip_address(generalConfig["host_pc"].get<string>());
    } catch (std::runtime_error &e) {
        cout << "Error: " << e.what() << endl;
        cout << "Exiting..." << endl;
        exit(1);
    }
    // char ipChar[input_ip_location.length() + 1];
    // strcpy(ipChar, input_ip_location.c_str());
    digitizerHandler digiHand = digitizerHandler(const_cast<char *>(input_ip_location.c_str()), generalConfig);

    // testing communication
    digiHand.test_communication();
    // NOTE : also configures the jumbo frame parameter (should it be outside the test_communication function ?
    digiHand.configure_ethernet_transmission();

    // Perform interface speed test (can be commented out if not needed)
    // digiHand.perform_interface_speedtest(4);
    if (params.runType == RunType::ShowConfig) {
        digiHand.configure(false);
        digiHand.dump_config();
        exit(0);
    } else if (params.runType == RunType::StopRun) {
        digiHand.stop_acquisition(false);
        exit(0);
    }

    digiHand.configure();
    std::time_t timeStartedAcquisition = std::time(nullptr);
    const uint8_t N_DIGITIZERS = digiHand.n_digitizers();
    std::vector<std::ofstream> ofstreams(N_DIGITIZERS);
    if (params.runType == RunType ::TimedRun) {
        for (size_t i = 0; i < ofstreams.size(); i++) {
            ofstreams[i].open(to_string(timeStartedAcquisition) + "_" + params.outputPath + "_" + to_string(i) + ".txt");
        }
    }

    UINT bltSize = generalConfig["readout"]["readout_blt"].get<UINT>();
    std::vector<size_t> eventSizes = digiHand.get_event_sizes();

    // Handles the readout of the digitizers
    std::vector<READOUT> readout_v(N_DIGITIZERS);
    // Keep track of monitoring values, not really needed for a standalone run, mostly here for emulating DAQling monitoring features.
    std::vector<Monitoring> monitoring_v(N_DIGITIZERS);
    for (size_t i = 0; i < N_DIGITIZERS; i++) {
        readout_v[i].eventSize = eventSizes[i];
        // creating the payload array with the maximum size a payload can have : size of one event * the number of events we want to read.
        // memory freed when READOUT destructor is called
        readout_v[i].raw_payload = new UINT[bltSize * eventSizes[i]];
    }

    digiHand.start_acquisition(false);
    Wait(1);

    if (params.runType == RunType::SoftwareTriggerRun) {
        // SOFTWARE TRIGGERS
        for (unsigned int i = 0; i < params.numberSwTriggers; i++) {
            digiHand.send_sw_trigger();
            Wait(0.002);  // Every 2 ms  === "500 Hz"
        }
    }

    // if true, stops the run loop
    bool stopDataAcquisition = false;
    // Stores the number of events present in the buffer of each digitizer
    std::vector<UINT> occupancies(N_DIGITIZERS);

    auto time_start = chrono::high_resolution_clock::now();
    // STARTING RUN LOOP
    while (!stopDataAcquisition) {
        try {
            digiHand.dump_event_count(occupancies);
        } catch (DigitizerHardwareException &e) {
            static int numErrors = 100;
            ERROR("Failed to read number of events: " << e.what());
            if (numErrors-- == 0) {
                ERROR("Too many read errors - bailing out");
                throw e;
            }
        }
        unsigned int n_events_to_do;
        // LOOPING WHILE ALL BUFFERS ARE NOT EMPTY
        while (!std::all_of(occupancies.begin(), occupancies.end(), [](int i) { return i == 0; })) {
            for (size_t d_id = 0; d_id < N_DIGITIZERS; d_id++) {
                // if one of the digitizers has no events, we skip it
                if (occupancies[d_id] == 0)
                    continue;

                n_events_to_do = std::min(occupancies[d_id], bltSize);
                READOUT &r = readout_v[d_id];
                Monitoring &m = monitoring_v[d_id];

                digiHand.read_single_event(d_id, r, n_events_to_do, false);
                if (r.nwordsObtained == 0) {
                    m.empty_events++;
                    if (m.empty_events >= 5) {
                        ERROR("Failed to retrieve at least 5 events - network connection unstable ?");
                    } else if (m.empty_events >= 50) {
                        ERROR("Failed to retrieve at least 50 events - problem ...");
                    }
                    continue;
                } else if (r.nwordsObtained < n_events_to_do * r.eventSize) {
                    cout << "Not enough words obtained." << endl;
                }
                if ((r.nwordsObtained != r.eventSize * n_events_to_do) && (r.nerrors == 0)) {
                    ERROR("Got " << r.nwordsObtained << " words while expecting " << r.eventSize * n_events_to_do << " words, but no errors ?");
                    r.nerrors = 1;
                }
                m.triggers += n_events_to_do;

                // WRITING TO FILE
                for (unsigned int i_evt = 0; i_evt < n_events_to_do; i_evt++) {
                    if (params.runType == RunType::TimedRun) {
                        write_to_file(ofstreams[d_id], r.raw_payload + i_evt * r.eventSize, r.eventSize);
                    }
                }
                occupancies[d_id] -= n_events_to_do;
            }  // end for loop over digitizers
        }      // end while lopp emptying buffers

        if (params.runType == RunType::SoftwareTriggerRun && std::any_of(monitoring_v.begin(), monitoring_v.end(), [&params](Monitoring &m) { return m.triggers >= params.numberSwTriggers; })) {
            auto time_stop = std::chrono::high_resolution_clock::now();
            double elapsed_time_ms = std::chrono::duration<double, std::milli>(time_stop - time_start).count();

            const double rate = params.numberSwTriggers / (elapsed_time_ms / 1000);
            cout << "Readout rate (combined) : " << rate << " Hz" << endl;
            stopDataAcquisition = true;
        } else if (params.runType == RunType::TimedRun) {
            if (std::time(nullptr) - timeStartedAcquisition > params.runTime) {
                stopDataAcquisition = true;
            }
        }
    }  // end loop run
    digiHand.stop_acquisition(false);
    if (params.runType == RunType::TimedRun) {
        for (auto &of : ofstreams) {
            of.close();
        }
    }

    return 0;
}
