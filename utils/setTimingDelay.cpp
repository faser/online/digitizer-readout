/*
  Copyright (C) 2019-2024 CERN for the benefit of the FASER collaboration
*/

#include "BOBR.h"
sis3153eth *gl_vme_crate;

vme_interface_class *intf;

int main(int argc, char *argv[])
{

	[[maybe_unused]] unsigned int addr;
	[[maybe_unused]] unsigned int data ;
	[[maybe_unused]] unsigned short ushort_data ;
	unsigned char cdata;
	cout << "usage: setTimingDelay [delay in ns]" << endl;

	   //char char_command[256];
	char  ip_addr_string[32] ;
	[[maybe_unused]] char ch_string[64] ;
	[[maybe_unused]] int int_ch ;
	int return_code ;

	[[maybe_unused]] char  pc_ip_addr_string[32] ;

	// default
	
	[[maybe_unused]] unsigned int vme_base_address = 0x00B00000 ;
	//FIXME: hardcoded IP address for digitizer
	strcpy(ip_addr_string,"10.11.65.39") ; // SIS3153 IP address

	int reg=0;
	bool write=false;
	double delay_value=0;
	if (argc>1) {
	  write=true;
	  delay_value=std::atof(argv[1]);
	}


	sis3153eth *vme_crate;
	sis3153eth(&vme_crate, ip_addr_string);

	char char_messages[128] ;
	unsigned int nof_found_devices ;

	// open Vme Interface device
	return_code = vme_crate->vmeopen();  // open Vme interface
	vme_crate->get_vmeopen_messages (char_messages, &nof_found_devices);  // open Vme interface

	BOBR *bobr = new BOBR(vme_crate,false);
	return_code = bobr->initPFC();
	if (return_code) {
	  printf("Failed to initialize PFC\n");
	  return 1;
	}

	return_code = bobr->readTTCRegister(reg,&cdata);
	if (return_code) {
	  printf("Failed to read TTC register\n");
	  return 1;
	}
	printf("\nRead TTC value in register %d: 0x%02X   rc=%02X\n",reg,cdata,return_code);
	int m=cdata&0x0F;
	int n=(cdata&0xF0)>>4;
	int K=(m*15+n*16+30)%240;
	printf(" Corresponds to K=%d (n=%d, m=%d), i.e. delay=%.3f ns\n",K,n,m,K*0.10417);
	

	if (write) {
	  K=(static_cast<int>(delay_value/0.10417))%240;
	  n=K%15;
	  m=(K/15-n+14)%16;
	  int nm=n*16+m;
	  printf("\nWrite 0x%02X to TTC register %d\n",nm,reg);
	  printf(" Corresponds to K=%d (n=%d, m=%d), i.e. delay=%.3f ns\n",K,n,m,K*0.10417);
	  [[maybe_unused]] unsigned int lock_status;
	  return_code=bobr->writeTTCRegister(reg,nm);
	  if (return_code) {
	    printf("Failed to write to TTC register\n");
	    return 1;
	  }
	  return_code = bobr->readTTCRegister(reg,&cdata);
	  if (return_code) {
	    printf("Failed to re-read TTC register\n");
	    return 1;
	  }
	  printf("Read back TTC value in register %d: 0x%02X   rc=%02X\n",reg,cdata,return_code);
	  m=cdata&0x0F;
	  n=(cdata&0xF0)>>4;
	  K=(m*15+n*16+30)%240;
	  printf(" Corresponds to K=%d (n=%d, m=%d), i.e. delay=%.3f ns\n",K,n,m,K*0.10417);
	}

	delete bobr;
	delete vme_crate;

	return 0;
}

