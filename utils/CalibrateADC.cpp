/*
  Copyright (C) 2019-2020 CERN for the benefit of the FASER collaboration
*/

/*!
 * \file CalibrateADC.cpp
 *
 * \author Sam Meehan
 * \date 13 Jan 2021
 *
 * \brief Calibrate ADCs for temperature swings
 *
 * \details Executable for performing calibration of ADC chips on vx1730 and vx1730s board.
 * \note ADC calibration is no longer needed for the vx1730s board before a run, it is done automatically at boot.
 */

#include <getopt.h>

#include <cstdlib>
#include <iomanip>
#include <iostream>

#include "Comm_vx1730.h"
#include "Helper.h"
#include "Helper_sis3153.h"

// parameters for the program
struct Params {
    std::string ip = "";
    std::string rotary = "";
};

int main(int argc, char *argv[]) {
    // used to tag the name of the program if you want in the output file
    std::string programName = argv[0];
    std::cout << "Program Running : " << programName << std::endl;
    std::size_t pos = programName.find("/");
    std::string programTag = programName.substr(pos + 1);
    std::cout << "Program Tag     : " << programTag << std::endl;

    ////////////////////////////////////////
    // Parse input arguments
    ////////////////////////////////////////

    int opt;
    Params params;
    while ((opt = getopt(argc, argv, "hi:r:")) != -1) {
        switch (opt) {
            case 'h':
                cout << "Help menu: \n \n";
                cout << std::left << std::setw(20) << "Option name" << std::setw(30) << "Argument" << "Description" << "\n";
                cout << std::setw(20) << "-----------" << std::setw(30) << "--------" << "----------" << "\n\n";
                cout << std::setw(20) << "-h :" << std::setw(30) << " " << "Prints the help message." << "\n";
                cout << std::setw(20) << "-i :" << std::setw(30) << "[IP_ADDR]" << "IP address or hostname." << "\n";
                cout << std::setw(20) << "-r :" << std::setw(30) << "[ROTARY]" << "Rotary address of the digitizer (configured on the board)" << "\n";
                cout << endl;
                exit(0);
                break;
            case 'i':
                params.ip = optarg;
                break;
            case 'r':
                params.rotary = optarg;
                break;
            case '?':
                cout << "Unknown option: " << optopt << endl;
                break;
        }
    }

    if (params.ip == "" || params.rotary == "") {
        std::cout << "********************************************" << std::endl;
        std::cout << "Incorrect Usage : " << std::endl;
        std::cout << "Please check usage via the [-h] help option" << std::endl;
        std::cout << "********************************************" << std::endl;
        return 1;
    }

    // correct IP address
    std::string ip_str = get_ip_address(params.ip);
    INFO("Returned IP Address : " << ip_str);

    // vme base address
    if (!correct_rotary_format(params.rotary)) {
        ERROR("You don't seem to have provided a correctly formatted HW address");
        ERROR("It should be of the format 0x12345678");
        exit(1);
    }

    UINT vme_base_address = std::stoul(params.rotary, 0, 16);
    INFO("IntValue : " << vme_base_address);
    INFO("Base VME Address = 0x" << std::setfill('0') << std::setw(8) << std::hex << vme_base_address);

    ////////////////////////////////////////
    // Load up the software instance of the digitizer control
    // This opens up the communication channels to the hardware itself
    ////////////////////////////////////////

    // needed because vx1730 class takes a char* as input, not const char*
    char *ip_addr_char = new char[ip_str.length() + 1];
    std::strcpy(ip_addr_char, ip_str.c_str());
    // make a new digitizer instance
    vx1730 *m_digitizer = new vx1730(ip_addr_char, vme_base_address);

    // test digitizer board interface
    sis3153_TestComm(m_digitizer->m_crate, true);

    INFO("Resetting board configuration");
    m_digitizer->Reset();

    // wait one second for temperature to stabilize if just turned on
    // documentation proposes at lease 100ms so this is fine
    INFO("Waiting for temperature to stabilize");
    Wait(1.0);

    // write to register that resets
    INFO("Performing calibration");
    m_digitizer->ADCCalibration();

    while (true) {
        INFO("Continuing calibration ... channel status [0 = not finished, 1 = finished] : ");

        bool calibration_finished = true;

        for (uint8_t ich = 0; ich < 16; ich++) {
            UINT data;
            ReadSlaveReg(m_digitizer->m_crate, m_digitizer->m_base_address + VX1730_CHANNEL_STATUS + (0x0100) * ich, data, false);
            INFO("ch " << ich << " : " << GetWordBit(data, 3));
            if (GetWordBit(data, 3) == 0)
                calibration_finished = false;
        }
        if (calibration_finished == true)
            break;

        //    Wait(1.0);
    }

    INFO("Calibration finished");
    return 0;
}
